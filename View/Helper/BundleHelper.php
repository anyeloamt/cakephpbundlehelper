<?php

App::uses('AppHelper', 'View/Helper');
App::uses('Html', 'View/Helper');

/**
 * This helper provides an easy way to bundle scripts and styles
 * for a CakePHP 2 Application.
 *
 * @author khodigolover
 * @package AmtBundleHelper
 * @link http://github.com/khodigolover/amtbundlehelper
 * @since version 1.1.1
 * @copyright Copyright (c) 2013 khodigolover
 *
 * Copyright (c) 2013 khodigolover
 *
 * MIT License
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */
class BundleHelper extends AppHelper {
    public $helpers = array('Html');

    /**
     * Takes a name and array of scripts to start with the bundle of the javascript files.
     *
     * @param string $name
     * @param array $scripts
     * @return void
     */
    public function makeScript($name, $scripts) {
        if (empty($scripts) or empty($name)) return;
        $this->constructBundle($name, $scripts, 'js');
    }

    /**
     * Takes a name and array of styles to start with the bundle of the css files.
     *
     * @param string $name
     * @param array $styles
     * @return void
     */
    public function makeStyle($name, $styles) {
        if (empty($styles) or empty($name)) return;
        $this->constructBundle($name, $styles, 'css');
    }

    /**
     * Takes an array of configuration with the scripts and styles to
     * make the process of bundling javascript and css files by one single method.
     *
     * @param array $config
     * @return void
     */
    public function make($config) {
        if (empty($config)) return;

        if (!empty($config['scripts'])) {
            $this->makeScript($config['scripts']['name'], $config['scripts']['files']);
        }
        if (!empty($config['styles'])) {
            $this->makeStyle($config['styles']['name'], $config['styles']['files']);
        }
    }

    /**
     * Takes the bundle of scripts created by the "makeScript" or "make" method, and
     * returns a script tag with src attribute pointing to the file's location.
     *
     * Requires make the bundle by calling the "make" or "makeScript" method before calling this.
     *
     * @see makeScript
     * @see make
     * @param string $name
     * @return string $htmlScriptTag
     */
    public function renderScript($name) {
        return $this->Html->script($name);
    }

    /**
     * Takes the bundle of styles created by the "makeStyle" or "make" method, and
     * returns a link tag with href attribute pointing to the file's location.
     *
     * Requires make the bundle by calling the "make" or "makeStyle" method before calling this.
     **
     * @see makeStyle
     * @see make
     * @param string $name
     * @return string $htmlLinkTag
     */
    public function renderStyle($name) {
        return $this->Html->css(array($name));
    }

    /**
     * Takes the name of the final bundle, an array with the names of the
     * files and the file type, searches for files in directory
     * "webroot/js" and "webroot/css" to take their content and store it in single file with the specified name.
     *
     * In addition calls the "minify" method to compile the contents of these.
     *
     * @see makeScript
     * @see make
     * @param string $name
     * @param array $files
     * @param string $type
     * @return void
     */
    protected function constructBundle($name, $files, $type) {
        $fileUrl = ($type == 'js') ? JS_URL : CSS_URL;
        $finalRender = '';

        foreach ($files as $file) {
            $file .= ".{$type}";
            $file = $fileUrl . $file;

            if (file_exists($file)) {
                $text = file_get_contents($file);
                $text = $this->minify($text);
                $finalRender .= $text;
            }
        }
        $name .= ".{$type}";
        file_put_contents($fileUrl . $name, $finalRender);
    }

    /**
     * Takes the text and compile it, eliminating the comments, tabs
     * and unnecessary spaces and returns it.
     *
     * @param string $text
     * @return string $text
     */
    public function minify($text) {
        $largeCommentsPattern = "^/\*(.)*\*/";
        $inlineCommentsPattern = "^(\s)*/{2}(.)*";
        $moreThanTwoSpacesPattern = "\s{2,}";

        $text = preg_replace("#$largeCommentsPattern#", "", $text);
        $text = preg_replace("#$inlineCommentsPattern#", "", $text);
        $text = str_replace("\n", "", $text);
        $text = str_replace("\t", "", $text);
        $text = preg_replace("#$moreThanTwoSpacesPattern#", " ", $text);

        return $text;
    }
}
