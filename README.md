CakePHPBundleHelper
=============================

This helper provides an easy way to bundle scripts and styles for a CakePHP 2 Application.
The main purpose is to take a bundle of js and css files into a single one or ones, improving the load process of the page reducing the number of requests to the server.

Inspired by [ASP.NET Bundling and Minification](http://www.asp.net/mvc/tutorials/mvc-4/bundling-and-minification).

Features
-------------

* Bundle all your js and css files into a single one.
* Minify your files, so the pages load faster.

Convert this:
```html
	<script type="text/javascript" src="js/jquery.js"></script>
	<script type="text/javascript" src="js/bootstrap.js"></script>
	<script type="text/javascript" src="js/scripts.js"></script>
	<script type="text/javascript" src="js/tools.js"></script>
	<script type="text/javascript" src="js/shilly.js"></script>
	
	<link rel="stylesheet" type="text/css" href="css/bootstrap.css"/>
	<link rel="stylesheet" type="text/css" href="css/styles.css"/>
	<link rel="stylesheet" type="text/css" href="css/adds.css"/>
	<link rel="stylesheet" type="text/css" href="css/corners.css"/>
	<link rel="stylesheet" type="text/css" href="css/hi.css"/>
```
into this:
```html
  <script type="text/javascript" src="js/__scripts.min.js"></script>
  <link rel="stylesheet" type="text/css" href="css/my_styles.min.css"/>
```  

Usage
-----

###Loading the helper. 
The first thing needed to do is to add the helper to the project by copying the `BundleHelper.php` to `app/View/Helper` directory.

Then you can add the helper to the `AppController`:
```php
<?php
class AppController extends Controller {
    ...
    public $helper = array('Bundle');
    ...
}
```
or to the specific controller, just like this:

```php
<?php
class PostsController extends AppController {
    ...
    public $helper = array('Bundle');
    ...
}
```

###Uncluding files to the bundle.

Alter load the helper we need to specify the files that we want to include in the bundle, we can do it in the `app/View/Layouts/default.ctp` by calling the `Bundle::make()`, `Bundle::makeScript()` or` Bundle::makeStyle()` method:
```php
...
<head>
	...
	<?php 
	$this->Bundle->makeScript('my-scripts',
	    array(
	    	'jquery',
	    	'bootstrap',
	    	'scripts',
	    	'tools',
	    	'shilly'
	    )
	);
	
	$this->Bundle->makeStyle('my-styles',
	    array(
	    	'bootstrap',
	    	'styles',
	    	'adds',
	    	'corners',
	    	'hi'
	    )
	);
	?>
	<?php echo $this->Bundle->renderStyle('my-styles'); ?>
</head>
<body>
...
  <footer>
    <?php echo $this->Bundle->renderScript('my-scripts'); ?>
  </footer>
</body>

```
result:
```html
	<head>
		...
		<link rel="stylesheet" type="text/css" href="css/my-styles.css"/>
		...
	</head>
	<body>
		...
		<footer>
			<script type="text/javascript" src="js/my-scripts.js"></script>
		</footer>
	</body>
```
You can also make a bundle by calling the `Bundle::make()` method.

```php
...
<head>
<?php 
  $this->Bundle->make(
	array(
		'scripts' => array(
			'name' => 'my-scripts',
			'files' => array(
				'jquery',
				'bootstrap',
				'scripts',
				'tools',
				'shilly'
            )
        ),
		'styles' => array(
			'name' => 'my-styles',
			'files' => array(
				'bootstrap',
				'styles',
				'adds',
				'corners',
				'hi'
			)
		)
     )
);

?>
<?php echo $this->Bundle->renderStyle('my-styles'); ?>
</head>
<body>
...
  <footer>
    <?php echo $this->Bundle->renderScript('my-scripts'); ?>
  </footer>
</body>
...
```
result:
```html
	<head>
		...
		<link rel="stylesheet" type="text/css" href="css/my-styles.css"/>
		...
	</head>
	<body>
		...
		<footer>
			<script type="text/javascript" src="js/my-scripts.js"></script>
		</footer>
	</body>
```
License
-----------
Distributed under the MIT license.

